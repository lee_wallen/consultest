#!/bin/sh

echo "Make sure docker machine is running..."

RESP=`docker-machine active 2>&1`
echo "Active docker-machine = $RESP"
if [ "$RESP" == "No active host found" ]
then
	echo "Nothing running. Please start docker-machine, set the environment variables, and try again."
	exit 1
fi

echo "Create consul servers..."

# server
CONTAINERS=`docker ps  -aq --filter="name=node*"`

for container in $CONTAINERS
do
	docker stop $container && docker rm $container
done

docker run -d --name node1 -h node1 progrium/consul -server -bootstrap-expect 3
JOIN_IP="$(docker inspect -f '{{.NetworkSettings.IPAddress}}' node1)"
docker run -d --name node2 -h node2 progrium/consul -server -join $JOIN_IP
docker run -d --name node3 -h node3 progrium/consul -server -join $JOIN_IP

# client:
docker run -d -p 8400:8400 -p 8500:8500 -p 8600:53/udp --name node4 -h node4 progrium/consul -join $JOIN_IP

sleep 5

# create consul settings

curl -X PUT -d 'myredis1:6379:0' http://192.168.99.100:8500/v1/kv/candidate/test-app/source-redis-host > /dev/null
curl -X PUT -d 'myredis2:6379:1' http://192.168.99.100:8500/v1/kv/candidate/test-app/target-redis-host > /dev/null
curl -X PUT -d '10000' http://192.168.99.100:8500/v1/kv/candidate/test-app/max-keys > /dev/null

echo
echo
echo "Create redis servers..."

# redis clients

CONTAINERS=`docker ps  -aq --filter="name=redis*"`

for container in $CONTAINERS
do
	docker stop $container && docker rm $container
done

docker run --name redis1 -p 7379:6379 -d redis redis-server --appendonly yes
docker run --name redis2 -p 7380:6379 -d redis redis-server --appendonly yes

echo 
echo
echo "Start java container that uses consulcall jar..."
docker run -it --rm --name java1 -v $PWD/target:/usr/src/consulcall -w /usr/src/consulcall --link redis1:myredis1 --link redis2:myredis2 --link node4:myconsul java:8 java -jar consulcall-1.SNAPSHOT.jar


echo "Done."
