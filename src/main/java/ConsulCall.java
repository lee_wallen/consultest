import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * Created by leewallen on 7/24/16.
 */
public class ConsulCall {

    public static void main (String[] args) {
        try {

            ConfigProvider configProvider = new ConfigProvider("candidate");
            copyData(configProvider);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void copyData(ConfigProvider cp) throws Exception {
        System.out.println("Clearing data from the source redis host.");
        clearRedisData(cp.getSourceRedisHost(), cp.getSourceRedisPort(), cp.getSourceRedisDb());
        System.out.println("Clearing data from the target redis host.");
        clearRedisData(cp.getTargetRedisHost(), cp.getTargetRedisPort(), cp.getTargetRedisDb());
        System.out.println("Creating test data for the source redis host.");
        List<String> keys = createTestData(cp.getSourceRedisHost(), cp.getSourceRedisPort(), cp.getSourceRedisDb(), cp.getMaxKeys());
        System.out.println("Getting test data from the source redis host.");
        List<String> data = getRedisData(cp.getSourceRedisHost(), cp.getSourceRedisPort(), cp.getSourceRedisDb(), keys.toArray(new String[0]));
        System.out.println("Seeting test data in the target redis host.");
        setRedisData(cp.getTargetRedisHost(), cp.getTargetRedisPort(), cp.getTargetRedisDb(), data.toArray(new String[0]));
        System.out.println("Done copying test data from source to target.");
    }


    private static List<String> createTestData(String sourceHost, int sourcePort, int sourceDb, int maxKeys) throws Exception {
        HashMap<String, String> testData = getTestData(maxKeys);
        populateRedisWithTestData(sourceHost, sourcePort, sourceDb, testData);
        return new ArrayList<>(testData.keySet());
    }

    private static void populateRedisWithTestData(String redisHost, int redisPort, int redisDb, HashMap<String, String> testData) {
        try (Jedis jedis = new Jedis(redisHost, redisPort)) {
            jedis.select(redisDb);

            for(Map.Entry<String, String> entry : testData.entrySet()) {
                String[] msetData = getMsetString(entry.getKey(), entry.getValue());
                jedis.mset(msetData);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String[] getMsetString(String key, String value) {
        List<String> myvalues = new ArrayList<>();

        myvalues.add(key);
        myvalues.add(value);

        return myvalues.toArray(new String[0]);
    }

    private static HashMap<String, String> getTestData(int numKeys) {
        Random rand = new Random();
        HashMap<String, String> testData = new HashMap<>(numKeys);

        for (int i = 0; i < numKeys; i++) {
            String key = UUID.randomUUID().toString();
            testData.put(key, String.valueOf(rand.nextInt()));
        }
        return testData;
    }



    private static void clearRedisData(String redisHost, int redisPort, int redisDb) throws Exception {
        try (Jedis jedis = new Jedis(redisHost, redisPort)) {
            jedis.select(redisDb);
            jedis.flushDB();
        }
    }

    private static void setRedisData(String redisHost, int redisPort, int redisDb, String[] vals) throws Exception {
        try (Jedis jedis = new Jedis(redisHost, redisPort)) {
            jedis.select(redisDb);
            String response = jedis.mset(vals);
            if (!response.equalsIgnoreCase("OK")) {
                throw new Exception ("There was a problem saving data to target redis.");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static List<String> getRedisData(String redisHost, int redisPort, int redisDb, String... mget) {
        List<String> vals = new ArrayList<>();
        try (Jedis jedis = new Jedis(redisHost, redisPort)) {
            jedis.select(redisDb);
            List<String> redisVals = jedis.mget(mget);
            int idx = 0;
            for (String key : mget) {
                String redisVal = redisVals.get(idx++);
                if (redisVal != null) {
                    vals.add(key);
                    vals.add(redisVal);
                }
            }
        }

        return vals;
    }
}
