import com.google.gson.Gson;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Base64;

/**
 * Created by leewallen on 7/24/16.
 */
public class ConfigProvider {

    private String sourceRedisHost;
    private int sourceRedisPort;
    private int sourceRedisDb;

    private String targetRedisHost;
    private int targetRedisPort;
    private int targetRedisDb;

    private int maxKeys;

    private static final String CONSUL = "http://myconsul:8500/v1/kv/%s/%s/%s";

    public ConfigProvider(String environment) {
        try {
            String sourceHost = getValue(environment, "test-app", "source-redis-host");
            String targetHost = getValue(environment, "test-app", "target-redis-host");

            String[] sourceValues = sourceHost.split(":");
            this.sourceRedisHost = sourceValues[0];
            this.sourceRedisPort = Integer.valueOf(sourceValues[1]);
            this.sourceRedisDb = Integer.valueOf(sourceValues[2]);

            String[] targetValues = targetHost.split(":");
            this.targetRedisHost = targetValues[0];
            this.targetRedisPort = Integer.valueOf(targetValues[1]);
            this.targetRedisDb = Integer.valueOf(targetValues[2]);

            this.maxKeys = getIntValue(environment, "test-app", "max-keys");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getValue(String environment, String app, String key) throws IOException {
        String json = readJsonFromUrl(String.format(CONSUL, environment, app, key));
        Gson gson = new Gson();
        ConfigValue[] config = gson.fromJson(json, ConfigValue[].class);

        String decoded = new String(Base64.getDecoder().decode(config[0].getValue()));
        return decoded;
    }

    private static int getIntValue(String environment, String app, String key) throws IOException {
        String json = readJsonFromUrl(String.format(CONSUL, environment, app, key));
        Gson gson = new Gson();
        ConfigValue[] config = gson.fromJson(json, ConfigValue[].class);

        String decoded = new String(Base64.getDecoder().decode(config[0].getValue()));
        return Integer.valueOf(decoded);
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }


    public static String readJsonFromUrl(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            return jsonText;
        } finally {
            is.close();
        }
    }

    public String getSourceRedisHost() { return sourceRedisHost; }
    public int getSourceRedisPort() { return sourceRedisPort; }
    public int getSourceRedisDb() { return sourceRedisDb; }

    public String getTargetRedisHost() { return targetRedisHost; }
    public int getTargetRedisPort() { return targetRedisPort; }
    public int getTargetRedisDb() { return targetRedisDb; }

    public int getMaxKeys() { return maxKeys; }
}
