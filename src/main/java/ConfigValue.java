import com.google.gson.annotations.SerializedName;

/**
 * Created by leewallen on 7/24/16.
 */
public class ConfigValue {
    //[{"CreateIndex":25,"ModifyIndex":25,"LockIndex":0,"Key":"candidate/test-app/batch-size","Flags":0,"Value":"NTAwMA=="}]

    @SerializedName("CreateIndex")
    private int createIndex;
    @SerializedName("ModifyIndex")
    private int modifyIndex;
    @SerializedName("LockIndex")
    private int lockIndex;
    @SerializedName("Key")
    private String key;
    @SerializedName("Flags")
    private int flags;
    @SerializedName("Value")
    private String value;


    public String getValue() { return value; }
    public void setValue(String value) { this.value = value; }

    public int getCreateIndex() {
        return createIndex;
    }

    public void setCreateIndex(int createIndex) {
        this.createIndex = createIndex;
    }

    public int getModifyIndex() {
        return modifyIndex;
    }

    public void setModifyIndex(int modifyIndex) {
        this.modifyIndex = modifyIndex;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public int getLockIndex() {
        return lockIndex;
    }

    public void setLockIndex(int lockIndex) {
        this.lockIndex = lockIndex;
    }
}
